package com.melipia94.appium_project.definition;

import com.melipia94.appium_project.pageobjects.PageDefault;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
public class DefDefault {
	PageDefault pagePagaFact;
	
	public void estoyEnLaPaginaDeInicio() {
		
	}

	@Given("^Estoy en la página de inicio$")
	public void estoy_en_la_página_de_inicio() {
//		MyCustomDriver driver = null;
//		driver.newDriver();
		
		estoyEnLaPaginaDeInicio();
	}
	@Given("^doy clic en \"([^\"]*)\"$")
		public void doy_clic_en(String objeto) {
			pagePagaFact.clic(objeto);
	  
	}
	@Then("^el campo \"([^\"]*)\" tiene el texto \"(.*)\"$")
		public void elCampoTieneElTexto(String objeto, String textoEsperado) {
			pagePagaFact.compararTxt(objeto, textoEsperado);
	}

}
