package com.melipia94.appium_project;
import com.melipia94.appium_project.utilities.ActionsUtil;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.android.AndroidDriver;
import net.thucydides.core.webdriver.DriverSource;
public class MyCustomDriver implements DriverSource{
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverSource.class);
	
	@Override
	public WebDriver newDriver() {
		WebDriver driver=null;
		try {
    
          
			DesiredCapabilities capabilities = new DesiredCapabilities();
	        capabilities.setCapability("deviceName", "Google Pixel");
	        capabilities.setCapability(CapabilityType.PLATFORM_NAME, "Android");
	        URL url = new URL ("http://0.0.0.0:4723/wd/hub");
	        URLConnection urlConnection = url.openConnection();
	        capabilities.setCapability("app", "src/test/resources/PreciseUnitConversion.apk");
	        driver = new AndroidDriver(urlConnection.getURL(), 
                    capabilities);
	        System.out.println("SetUp is successful and Appium Driver is launched successfully");   


		}catch (Exception e) {
				LOGGER.error("Excepcion Custom Driver: ", e);
		}
		return driver;
	}

	@Override
	public boolean takesScreenshots() {
		return true;
	}
}